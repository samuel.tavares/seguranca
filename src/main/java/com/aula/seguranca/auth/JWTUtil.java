package com.aula.seguranca.auth;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JWTUtil {

    private static final String SEGREDO = "541vs1vs168rv6s8r4rvs84vs1r4vvs64svsrr8vs616984s90lp0q8za9caa4kcvkojan";
    private static final Long TEMPO_DE_VALIDADE = 86000l;

    public String gerarToken(String email){
        Date dataDeVencimento = new Date(System.currentTimeMillis()+TEMPO_DE_VALIDADE);
        String token = Jwts.builder().setSubject(email)
                .setExpiration(dataDeVencimento)
                .signWith(SignatureAlgorithm.HS512, SEGREDO.getBytes())
                .compact();

        return token;
    }

    public String getEmail(String token){
        Claims claims = getClaims(token);
        String email = claims.getSubject();
        return email;
    }

    public Claims getClaims(String token){
        return Jwts.parser().setSigningKey(SEGREDO.getBytes()).parseClaimsJws(token).getBody();
    }

    public boolean tokenValido(String token){
        Claims claims = getClaims(token);
        String email = claims.getSubject();

        Date dataDeExpiracao = claims.getExpiration();
        Date dataAtual = new Date(System.currentTimeMillis());

        if(email != null && dataDeExpiracao != null && dataAtual.before(dataDeExpiracao)){
            return true;
        }else{
            return false;
        }
    }

}
