package com.aula.seguranca.controllers;

import com.aula.seguranca.models.Usuario;
import com.aula.seguranca.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario registrarUsuario(@RequestBody @Valid Usuario usuario){
        return usuarioService.salvarUsuario(usuario);
    }

    @GetMapping
    public Iterable<Usuario> listarUsuarios(){
        return usuarioService.lerTodosUsuarios();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarUsuario(@PathVariable (name = "id") long id){
        usuarioService.deletarUsuario(id);
    }
}
